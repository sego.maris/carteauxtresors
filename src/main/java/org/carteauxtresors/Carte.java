package org.carteauxtresors;

import java.awt.*;
import java.util.ArrayList;
import java.util.Map;


public class Carte {

   private final int tailleHorizontal;

   private final int tailleVerticale;

   private Map<Point, Montagne> listMontagne;

   private Map<Point, Tresor> listTresor;

   private Map<String, Aventurier> listAventurier;


   private ArrayList<String> listNomAventurier;

    public Carte(int tailleHorizontal, int tailleVerticale) {
        this.tailleHorizontal = tailleHorizontal;
        this.tailleVerticale = tailleVerticale;
    }

    /**
     * getter pour l'attribut tailleHorizontal
     * @return tailleHorizontal
     */
    public int getTailleHorizontal() {
        return tailleHorizontal;
    }

    /**
     * getter pour l'attribut tailleVerticale
     * @return tailleVerticale
     */
    public int getTailleVerticale() {
        return tailleVerticale;
    }

    /**
     * getter pour l'attribut listMontagne
     * @return listMontagne
     */
    public Map<Point, Montagne> getListMontagne() {
        return listMontagne;
    }

    /**
     * setter pour l'attribut listMontagne
     * @param listMontagne
     */
    public void setListMontagne(Map<Point, Montagne> listMontagne) {
        this.listMontagne = listMontagne;
    }

    /**
     * getter pour l'attribut listTresor
     * @return listTresor
     */
    public Map<Point, Tresor> getListTresor() {
        return listTresor;
    }

    /**
     * setter pour l'attribut listTresor
     * @param listTresor
     */
    public void setListTresor(Map<Point, Tresor> listTresor) {
        this.listTresor = listTresor;
    }

    /**
     * getter pour l'attribut listAventurier
     * @return listAventurier
     */
    public Map<String, Aventurier> getListAventurier() {
        return listAventurier;
    }

    /**
     * setter pour l'attribut listAventurier
     * @param listAventurier
     */
    public void setListAventurier(Map<String, Aventurier> listAventurier) {
        this.listAventurier = listAventurier;
    }

    /**
     * getter pour l'attribut listNomAventurier
     * @return listNomAventurier
     */
    public ArrayList<String> getListNomAventurier() {
        return listNomAventurier;
    }

    /**
     * setter pour l'attribut listNomAventurier
     * @param listNomAventurier
     */
    public void setListNomAventurier(ArrayList<String> listNomAventurier) {
        this.listNomAventurier = listNomAventurier;
    }

    /**
     * méthode permettant de lancer la chasse aux trésors
     */
    public void lancerChasseAuxTresors(){
        ArrayList<String> listeAventurier = this.getListNomAventurier();
        Map<String, Aventurier> mapAventurier = this.getListAventurier();


        do {
            for(String nomAventurier : listeAventurier){
                Aventurier aventurier = mapAventurier.get(nomAventurier);
                if(!aventurier.getSequenceDeMouvement().isEmpty()){
                    aventurier.deplacement(this);
                }else{
                    listeAventurier.remove(nomAventurier);
                    break;
                }
            }
        } while (!listeAventurier.isEmpty());
    }
}
