package org.carteauxtresors;

public enum Orientation {

    N,
    S,
    E,
    O;
}
