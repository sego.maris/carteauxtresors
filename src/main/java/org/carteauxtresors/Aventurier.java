package org.carteauxtresors;

import java.awt.*;
import java.util.Map;
import java.util.Objects;

public class Aventurier {

    private String nomAventurier;
    private Point positionAventurier;
    private Orientation orientationAventurier;
    private String sequenceDeMouvement;

    private int nbTresorRamasse;

    public Aventurier(String nomAventurier, Point positionAventurier, Orientation orientationAventurier, String sequenceDeMouvement, int nbTresorRamasse) {
        this.nomAventurier = nomAventurier;
        this.positionAventurier = positionAventurier;
        this.orientationAventurier = orientationAventurier;
        this.sequenceDeMouvement = sequenceDeMouvement;
        this.nbTresorRamasse = nbTresorRamasse;
    }

    /**
     * getter pour l'attribut nomAventurier
     * @return nomAventurier
     */
    public String getNomAventurier() {
        return nomAventurier;
    }

    /**
     * setter pour l'attribut nomAventurier
     * @param nomAventurier
     */
    public void setNomAventurier(String nomAventurier) {
        this.nomAventurier = nomAventurier;
    }

    /**
     * getter pour l'attribut positionAventurier
     * @return positionAventurier
     */
    public Point getPositionAventurier() {
        return positionAventurier;
    }

    /**
     * setter pour l'attribut positionAventurier
     * @param positionAventurier
     */
    public void setPositionAventurier(Point positionAventurier) {
        this.positionAventurier = positionAventurier;
    }

    /**
     * getter pour l'attribut orientationAventurier
     * @return orientationAventurier
     */
    public Orientation getOrientationAventurier() {
        return orientationAventurier;
    }

    /**
     * setter pour l'attribut orientationAventurier
     * @param orientationAventurier
     */
    public void setOrientationAventurier(Orientation orientationAventurier) {
        this.orientationAventurier = orientationAventurier;
    }

    /**
     * getter pour l'attribut sequenceDeMouvement
     * @return sequenceDeMouvement
     */
    public String getSequenceDeMouvement() {
        return sequenceDeMouvement;
    }

    /**
     * setter pour l'attribut sequenceDeMouvement
     * @param sequenceDeMouvement
     */
    public void setSequenceDeMouvement(String sequenceDeMouvement) {
        this.sequenceDeMouvement = sequenceDeMouvement;
    }

    /**
     * getter pour l'attribut nbTresorRamasse
     * @return nbTresorRamasse
     */
    public int getNbTresorRamasse() {
        return nbTresorRamasse;
    }

    /**
     * setter pour l'attribut nbTresorRamasse
     * @param nbTresorRamasse
     */
    public void setNbTresorRamasse(int nbTresorRamasse) {
        this.nbTresorRamasse = nbTresorRamasse;
    }

    /**
     * méthode permettant le déplacement d'un aventurier
     * @param carte
     */
    public void deplacement(Carte carte){
        Point positionAventurirer = this.getPositionAventurier();
        Orientation orientation = this.getOrientationAventurier();
        String sequence = this.getSequenceDeMouvement();
        char mouvement = sequence.charAt(0);
        Map<Point, Montagne> listMontagnes = carte.getListMontagne();
        Map<Point, Tresor> listTresors = carte.getListTresor();
        Map<String, Aventurier> listAventurier = carte.getListAventurier();

        Point newPosition = new Point();
        switch (mouvement){
            case 'A':
                switch (orientation){
                    case N:
                        newPosition.setLocation(positionAventurirer.getX(), positionAventurirer.getY()-1);
                        if(isInMap(newPosition, carte)) {
                            if(isMontagne(newPosition, listMontagnes))break;
                            if(isFree(newPosition, listAventurier))break;
                            isTresor(newPosition, listTresors);
                            this.setPositionAventurier(newPosition);
                            break;
                        }
                    case S:
                        newPosition.setLocation(positionAventurirer.getX(), positionAventurirer.getY()+1);
                        if(isInMap(newPosition, carte)) {
                            if(isMontagne(newPosition, listMontagnes))break;
                            if(isFree(newPosition, listAventurier))break;
                            isTresor(newPosition, listTresors);
                            this.setPositionAventurier(newPosition);
                            break;
                        }
                    case E:
                        newPosition.setLocation(positionAventurirer.getX() +1, positionAventurirer.getY());
                        if(isInMap(newPosition, carte)) {
                            if(isMontagne(newPosition, listMontagnes))break;
                            if(isFree(newPosition, listAventurier))break;
                            isTresor(newPosition, listTresors);
                            this.setPositionAventurier(newPosition);
                            break;
                        }
                    case O:
                        newPosition.setLocation(positionAventurirer.getX() -1, positionAventurirer.getY());
                        if(isInMap(newPosition, carte)) {
                            if(isMontagne(newPosition, listMontagnes))break;
                            if(isFree(newPosition, listAventurier))break;
                            isTresor(newPosition, listTresors);
                            this.setPositionAventurier(newPosition);
                            break;
                        }
                }
                break;

            case 'G':
                switch (orientation){

                    case N -> this.setOrientationAventurier(Orientation.O);
                    case S -> this.setOrientationAventurier(Orientation.E);
                    case E -> this.setOrientationAventurier(Orientation.N);
                    case O -> this.setOrientationAventurier(Orientation.S);
                }
                break;

            case 'D':
                switch (orientation){

                    case N -> this.setOrientationAventurier(Orientation.E);
                    case S -> this.setOrientationAventurier(Orientation.O);
                    case E -> this.setOrientationAventurier(Orientation.S);
                    case O -> this.setOrientationAventurier(Orientation.N);
                }
                break;

            default:
                break;

        }
        this.setSequenceDeMouvement(sequence.substring(1));

    }

    /**
     * Est-ce que le mouvement nous fait sortir de la carte
     * @param position
     * @param carte
     * @return
     */
    public boolean isInMap(Point position, Carte carte){
        return (((0<= position.getX()) && (position.getX()<carte.getTailleHorizontal())) && ((0<=position.getY())&& (position.getY()<carte.getTailleVerticale())));
    }

    /**
     * Est-ce que cette case correspond à une case montagne
     * @param newPosition
     * @param listMontagnes
     * @return
     */
    public boolean isMontagne(Point newPosition , Map<Point, Montagne> listMontagnes){
        for (Point point : listMontagnes.keySet()) {
            if (newPosition.equals(point)) return true;
        }return false;
    }

    /**
     * Est-ce qu'il y a déjà quelqu'un sur cette case
     * @param newPosition
     * @param listAventurier
     * @return
     */
    public boolean isFree(Point newPosition , Map<String, Aventurier> listAventurier){
        for (Aventurier aventurier : listAventurier.values()) {
            if (newPosition.equals(aventurier.getPositionAventurier())) return true;
        }return false;
    }

    /**
     * Est-ce que cette case correspond à une case trésor
     * @param newPosition
     * @param listTresors
     */
    public void isTresor(Point newPosition, Map<Point, Tresor> listTresors){
        for (Point point : listTresors.keySet()) {
            if (newPosition.equals(point)) {
                Tresor tresor = listTresors.get(point);
                int nbTresor = tresor.getNbTresor();
                if (nbTresor > 0) {
                    nbTresor--;
                    tresor.setNbTresor(nbTresor);
                    nbTresorRamasse++;
                }
            }
        }
    }

    /**
     * méthode equals
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aventurier that = (Aventurier) o;
        return nbTresorRamasse == that.nbTresorRamasse && Objects.equals(nomAventurier, that.nomAventurier) && Objects.equals(positionAventurier, that.positionAventurier) && orientationAventurier == that.orientationAventurier && Objects.equals(sequenceDeMouvement, that.sequenceDeMouvement);
    }

    /**
     * méthode hashCode
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(nomAventurier, positionAventurier, orientationAventurier, sequenceDeMouvement, nbTresorRamasse);
    }
}
