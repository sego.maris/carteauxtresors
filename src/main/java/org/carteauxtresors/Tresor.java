package org.carteauxtresors;

import java.awt.*;

public class Tresor {

    private Point coordonnee;

    private int nbTresor;

    public Tresor(Point coordonnée, int nbTresor) {
        this.coordonnee = coordonnée;
        this.nbTresor = nbTresor;
    }

    /**
     * getter pour l'attribut coordonnee
     * @return coordonnee
     */
    public Point getCoordonnee() {
        return coordonnee;
    }

    /**
     * setter pour l'attribut coordonnee
     * @param coordonnee
     */
    public void setCoordonnee(Point coordonnee) {
        this.coordonnee = coordonnee;
    }

    /**
     * getter pour l'attribut nbTresor
     * @return nbTresor
     */
    public int getNbTresor() {
        return nbTresor;
    }

    /**
     * setter pour l'attribut nbTresor
     * @param nbTresor
     */
    public void setNbTresor(int nbTresor) {
        this.nbTresor = nbTresor;
    }
}
