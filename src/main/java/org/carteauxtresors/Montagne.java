package org.carteauxtresors;

import java.awt.*;
public class Montagne {

    private Point coordonneeMontagne;

    public Montagne(Point coordonneeMontagne) {
        this.coordonneeMontagne = coordonneeMontagne;
    }

    /**
     * getter pour l'attribut coordonneeMontagne
     * @return coordonneeMontagne
     */
    public Point getCoordonneeMontagne() {
        return coordonneeMontagne;
    }

    /**
     * setter pour l'attribut coordonneeMontagne
     * @param coordonneeMontagne
     */
    public void setCoordonneeMontagne(Point coordonneeMontagne) {
        this.coordonneeMontagne = coordonneeMontagne;
    }
}
