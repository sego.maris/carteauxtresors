package org.carteauxtresors;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Init {

    private static final String RESOURCES_PATH = "src/test/resources/";
    public static final String fichierEntree = RESOURCES_PATH + "FichierEntree.txt";


    private static final Logger LOG = LogManager.getLogger(Init.class);

    public static Carte initialiserCarte (File fichierEntree){

        if(fichierEntree == null)return null;
        boolean carteOk = verifierCarte(fichierEntree);
        if(!carteOk) return null;
        Carte carte  = creerCarte(fichierEntree);
        if (remplirCarte(carte, fichierEntree)) return carte;
        return null;
    }

    /**
     * verifie que le fichier d'eentrée existe
     * @return file
     */
    public static File verifierFichier(){

        File file = new File(fichierEntree);



        if(!file.exists()){
            return null;
        }

        return file;
    }

    /**
     * verifie qu'il existe bien au moins une carte, un avanturier et un trésor
     * @param file
     * @return boolean
     */
    private static boolean verifierCarte(File file){

        boolean carteExist = false;
        boolean aventurierExist = false;
        boolean tresorExist = false;

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader  = new BufferedReader(fileReader);
            String line;


            while((line = bufferedReader.readLine()) != null) {
                if(line.startsWith("A -")) aventurierExist = true;
                if(line.startsWith("C -")) carteExist = true;
                if(line.startsWith("T -")) tresorExist = true;
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if(!aventurierExist) LOG.info("il n'existe pas d'aventurier dans le fichier d'entrée");
        if(!carteExist) LOG.info("il n'existe pas de carte dans le fichier d'entrée");
        if(!tresorExist) LOG.info("il n'existe pas de trésor dans le fichier d'entrée");


        return (aventurierExist && carteExist && tresorExist);

    }

    /**
     * creation de la carte
     * @param file
     * @return carte
     */
    private static Carte creerCarte(File file) {
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            Carte carte = null;

            while ((line = bufferedReader.readLine()) != null) {

                if (line.startsWith("C -")) {
                    String[] carteTab = line.split(" - ");

                     carte = new Carte(Integer.parseInt(carteTab[1]), Integer.parseInt(carteTab[2]));

                }
            }fileReader.close();
            bufferedReader.close();

            return carte;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * creation des avanturiers, montagne et trésors et verification qu'ils se trouvent dans la carte
     * @param carte
     * @param file
     * @return boolean
     */
    private static boolean remplirCarte(Carte carte, File file){

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            Map<Point, Montagne> mapMontagne = new HashMap<>();

            Map<Point, Tresor> mapTresor = new HashMap<>();

            Map<String, Aventurier> maptAventurier = new HashMap<>();

            ArrayList<String> listNomAventurier = new ArrayList<String>();

            while ((line = bufferedReader.readLine()) != null) {

                if (line.startsWith("A -")) {
                    String[] aventurierTab = line.split(" - ");

                    Point positionAventurier = new Point(Integer.parseInt(aventurierTab[2]), Integer.parseInt(aventurierTab[3]));
                    if(positionAventurier.getX()< carte.getTailleHorizontal() && positionAventurier.getY()< carte.getTailleVerticale()){
                        Orientation orientationAventurier = Orientation.valueOf(aventurierTab[4]);
                        Aventurier aventurier = new Aventurier(aventurierTab[1], positionAventurier, orientationAventurier, aventurierTab[5], 0);

                        listNomAventurier.add(aventurier.getNomAventurier());
                        maptAventurier.put(aventurier.getNomAventurier(), aventurier);
                    }

                }

                if (line.startsWith("T -")) {
                    String[] tresorTab = line.split(" - ");

                    Point positionTresor = new Point(Integer.parseInt(tresorTab[1]), Integer.parseInt(tresorTab[2]));
                    if(positionTresor.getX()<carte.getTailleHorizontal() && positionTresor.getY()<carte.getTailleVerticale()) {
                        Tresor tresor = new Tresor(positionTresor, Integer.parseInt(tresorTab[3]));

                        mapTresor.put(positionTresor, tresor);
                    }
                }

                if (line.startsWith("M -")) {
                    String[] montagneTab = line.split(" - ");

                    Point positionMontagne = new Point(Integer.parseInt(montagneTab[1]), Integer.parseInt(montagneTab[2]));
                    if(positionMontagne.getX()<carte.getTailleHorizontal() && positionMontagne.getY()<carte.getTailleVerticale()){
                        Montagne montagne = new Montagne(positionMontagne);

                        mapMontagne.put(positionMontagne, montagne);
                    }

                }
            }

            fileReader.close();
            bufferedReader.close();


            carte.setListNomAventurier(listNomAventurier);
            carte.setListAventurier(maptAventurier);
            carte.setListMontagne(mapMontagne);
            carte.setListTresor(mapTresor);

            return !carte.getListAventurier().isEmpty() && !carte.getListTresor().isEmpty();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * création et écriture du fihier de sortie
     * @param carte
     */
    public static void initFichierSortie(Carte carte){
        File fichierSortie = new File(RESOURCES_PATH + "FichierSortie.txt");

        if(fichierSortie.exists()){
            fichierSortie.delete();
        }

        try {
            fichierSortie.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            FileWriter fileWritter = new FileWriter(fichierSortie);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWritter);

            bufferedWriter.write("C - " + (int)carte.getTailleHorizontal() +  " - " + (int)carte.getTailleVerticale());

            if(carte.getListMontagne() != null) {
                for (Montagne montagne : carte.getListMontagne().values()) {
                    bufferedWriter.newLine();
                    bufferedWriter.write("M - " + (int) montagne.getCoordonneeMontagne().getX() + " - " + (int) montagne.getCoordonneeMontagne().getY());
                }
            }

            if(carte.getListTresor()!=null){
                for(Tresor tresor:  carte.getListTresor().values()) {
                    if (tresor.getNbTresor() > 0) {
                        bufferedWriter.newLine();
                        bufferedWriter.write("T - " + (int) tresor.getCoordonnee().getX() + " - " + (int) tresor.getCoordonnee().getY() + " - " + tresor.getNbTresor());
                    }
                }
            }


            if(carte.getListAventurier()!=null) {
                for (Aventurier aventurier : carte.getListAventurier().values()) {
                    bufferedWriter.newLine();
                    bufferedWriter.write("A - " + aventurier.getNomAventurier() + " - " + (int) aventurier.getPositionAventurier().getX() + " - " + (int) aventurier.getPositionAventurier().getY() + " - " + aventurier.getOrientationAventurier() + " - " + aventurier.getNbTresorRamasse());
                }
            }

            bufferedWriter.flush();
            fileWritter.close();
            bufferedWriter.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
