package org.carteauxtresors;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

public class Main {
    private static final Logger LOG = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        File fichierEntree = Init.verifierFichier();

        if(fichierEntree != null) {

            Carte carte = Init.initialiserCarte(fichierEntree);

            if (carte == null) {
                LOG.info("problème lors de l'initialisation de la carte");
            } else {

                carte.lancerChasseAuxTresors();

                Init.initFichierSortie(carte);
            }

        }else{
            LOG.warn("Le fichier d'entrée est inexistant");
        }

    }
}