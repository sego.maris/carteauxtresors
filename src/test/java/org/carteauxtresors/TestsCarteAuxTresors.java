package org.carteauxtresors;

import org.junit.jupiter.api.Test;
import java.awt.*;
import java.io.*;
import static org.junit.jupiter.api.Assertions.*;
public class TestsCarteAuxTresors {

    private static final String RESOURCES_PATH = "src/test/resources/";

    /**
     * on teste la création du fichier de sortie
     */
    @Test
    public void fichierSortieCree(){

        Carte carte = new Carte(1,1);
        Init.initFichierSortie(carte);

        File fichierSortie = new File(RESOURCES_PATH + "FichierSortie.txt");

        assertNotNull(fichierSortie);
    }

    /**
     * on teste que les avanturiers ne peuvent aller sur une case montagne
     */
    @Test
    public void stopAuxMontagnes(){

        File file  = new File(RESOURCES_PATH + "StopMontagne.txt");
        Carte carte = Init.initialiserCarte(file);
        carte.lancerChasseAuxTresors();
        Aventurier alan = carte.getListAventurier().get("Alan");
        Point pointExpected = new Point(1, 1);
        assertEquals(pointExpected, alan.getPositionAventurier());
    }

    /**
     * on teste qu'il ne peut y avoir qu'une personne par case
     */
    @Test
    public void unePersonneParCase(){
        File file  = new File(RESOURCES_PATH + "UnePersonneParCase.txt");
        Carte carte = Init.initialiserCarte(file);
        carte.lancerChasseAuxTresors();
        Aventurier alan = carte.getListAventurier().get("Alan");
        Point pointExpected = new Point(1, 1);
        assertEquals(pointExpected, alan.getPositionAventurier());
    }

    /**
     * on teste que les informations récupérer lors de la création de la carte sont corrects
     */
    @Test
    public void recuperationInfoCorrect(){
        File file  = new File(RESOURCES_PATH + "RecuperationInfoCorrect.txt");
        Carte carte = Init.initialiserCarte(file);
        Point position = new Point(1, 1);
        Aventurier alan = carte.getListAventurier().get("Alan");
        Aventurier bety = new Aventurier("Alan", position, Orientation.S, "ADADADA", 0);
        assertEquals(bety, alan);

    }

    /**
     * on teste que les aventurier ne peuvent sortir de la carte
      */
    @Test
    public void sortieCarte(){
        File file  = new File(RESOURCES_PATH + "SortieCarte.txt");
        Carte carte = Init.initialiserCarte(file);
        carte.lancerChasseAuxTresors();
        Aventurier alan = carte.getListAventurier().get("Alan");
        Point pointExpected = new Point(0, 1);
        assertEquals(pointExpected, alan.getPositionAventurier());
    }

    /**
     * on teste que les informations dans le fichier de sortie sont corrects
     */
    @Test
    public void infoFichierSortie(){

        File file  = new File(RESOURCES_PATH + "InfoFichierSortie.txt");
        Carte carte = Init.initialiserCarte(file);
        carte.lancerChasseAuxTresors();
        Init.initFichierSortie(carte);

        try {
            FileReader fr = new FileReader(RESOURCES_PATH + "FichierSortie.txt");
            BufferedReader br = new BufferedReader(fr);
            String line1 = br.readLine();
            String line2 = br.readLine();
            String line3 = br.readLine();
            String line4 = br.readLine();
            String line5 = br.readLine();

            fr.close();
            br.close();

            assertEquals("C - 3 - 4", line1);
            assertEquals("M - 1 - 0", line2);
            assertEquals("M - 2 - 1", line3);
            assertEquals("T - 1 - 3 - 2", line4);
            assertEquals("A - Lara - 0 - 3 - S - 3", line5);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
